package set;

import java.util.Iterator;

/*
 * This class implement all those methods which same for all structures 
 */
public abstract class AbstractSet<T> implements SetADT<T> {

	/*
	 * add a new Set structure to the existing Set
	 */
	public boolean addAll(SetADT<T> A) {
		//if at least one element of Set A is added hasBeenModified will be true
		boolean hasBeenModified = false;
		//Go through Set A element and add to this Set
		for(T element : A) {
			if(this.add(element)) {
				hasBeenModified = true;
			}
		}
		return hasBeenModified;
	}
	
	/*
	 * removes elements which match with A Set structure
	 */
	public boolean removeAll(SetADT<T> A) {
		//if at least one element is removed from this Set hasBeenModified will be true
		boolean hasBeenModified = false;
		//Go through Set A element and add to this Set
		Iterator<T> iter = A.iterator();
		while(iter.hasNext()) {
			//remove element of Set A if is present in this Set
			if(this.remove(iter.next()))
				hasBeenModified = true;
		}
		return hasBeenModified;
	}
	
	/*
	 * checks if given Set A is subset of this Set
	 */
	public boolean isSubset(SetADT<T> A) {
		//check if this size is higher than Set A
		if(this.size() > A.size())
			return false;
		//Go through each element of this
		for(T element : this) {
			//check if this Set elements are on Set A
			if(!A.contains(element))
				return false;
		}
		return true;
	}
	
	/*
	 * To check whether the given set (i.e. A) contains exactly the same elements as this set object. . .
	 */
	public boolean equals(AbstractSetADT<T> A) {
		//check their sizes 
		if(this.size() != A.size()) 
			return false;
		return isSubset(A) && A.isSubset(this);
	}
	
}

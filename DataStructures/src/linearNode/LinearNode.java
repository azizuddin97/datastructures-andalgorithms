package linearNode;

/**
 * LinearNode.java
 * 
 * @author Aziz Uddin
 * @author S H S Wong
 * @param <T>
 */
public class LinearNode<T> {

	// element of node
	private T element;
	// node which follows this one
	private LinearNode<T> next;

	/**
	 * Constructor: create empty node
	 */
	public LinearNode() {
		next = null;
		element = null;
	}

	/**
	 * Constructor with parameter
	 * 
	 * @param elemnt which is stored in node
	 */
	public LinearNode(T element) {
		next = null;
		this.element = element;
	}

	/**
	 * Returns the node that follows this one.
	 * 
	 * @return The node that follows this one.
	 */
	public LinearNode<T> getNext() {
		return next;
	}

	/**
	 * Sets the node that follows this one.
	 * 
	 * @param node The node which is to follow this one.
	 */
	public void setNext(LinearNode<T> node) {
		next = node;
	}

	/**
	 * Returns the element stored in this node.
	 * 
	 * @return The element that is kept within this node.
	 */
	public T getElement() {
		return element;
	}

	/**
	 * Sets the element stored in this node.
	 * 
	 * @param elem The element that is to be kept within this node.
	 */
	public void setElement(T elem) {
		element = elem;
	}
}

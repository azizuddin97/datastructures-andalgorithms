package iterators;

import java.util.Iterator;

/*
 * ArrayIterator iterator which allows to iterate over the collection (Array)
 * 
 * ArrayIterator.java
 * 
 * @author Aziz Uddin
 * 
 * @author S H S Wong
 * 
 */
public class ArrayIterator<T> implements Iterator<T> {

	// the elements to be iterated over
	private T[] contents;

	// the index of the element to be returned in the next() operation
	private int cursor;

	// array limit (aka size)
	private int limit;

	public ArrayIterator(T[] array, int size) {
		contents = array;
		limit = size;
		cursor = 0;
	}

	// returns if next element is available
	@Override
	public boolean hasNext() {
		return cursor < limit;
	}

	// returns the next element from contents
	@Override
	public T next() {
		if (!hasNext())
			throw new IllegalStateException("ArrayIterator: no next element");
		else
			// this returns the current element and afterwards increments cursor++ [did not
			// know that ]
			return contents[cursor++];
	}

}

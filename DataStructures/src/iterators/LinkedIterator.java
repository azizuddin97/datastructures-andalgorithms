package iterators;

import java.util.Iterator;

import linearNode.LinearNode;

/**
 * LinkedIterator.java allows to iterate through LinkedList Structure
 * 
 * @author Aziz Uddin
 * @author S H S Wong
 * @param <T>
 */
public class LinkedIterator<T> implements Iterator<T> {

	// the current position in the linked list
	private LinearNode<T> cursor;

	// Sets up the cursor to the start of this linked list.
	public LinkedIterator(LinearNode<T> contents) {
		cursor = contents;
	}

	/**
	 * @return true if iterator has more elements in this iteration
	 */
	@Override
	public boolean hasNext() {
		return cursor != null;
	}

	/**
	 * @return T which is next element in this iteration
	 */
	@Override
	public T next() {
		if (!hasNext())
			throw new IllegalStateException("no next element");
		T result = cursor.getElement();
		cursor = cursor.getNext();
		return result;
	}

}

package testStructures;

import java.util.Iterator;

import set.LinkedSet;

public class Main {

	public static void main(String[] args) {
		LinkedSet<Integer> firstLinkedSet = new LinkedSet<Integer>();
		LinkedSet<Integer> secondLinkedSet = new LinkedSet<Integer>();
		LinkedSet<Integer> thirdLinkedSet = new LinkedSet<Integer>();

		firstLinkedSet.add(10);
		firstLinkedSet.add(20);
		firstLinkedSet.add(30);
		firstLinkedSet.add(40);
		firstLinkedSet.add(60);
		secondLinkedSet.add(10);
		secondLinkedSet.add(20);
		secondLinkedSet.add(30);
		secondLinkedSet.add(40);
		secondLinkedSet.add(50);
		secondLinkedSet.add(100);
		secondLinkedSet.add(200);
		secondLinkedSet.add(300);
		secondLinkedSet.add(400);
		secondLinkedSet.add(500);

//		thirdLinkedSet = firstLinkedSet.intresection(secondLinkedSet);
		thirdLinkedSet = firstLinkedSet.difference(secondLinkedSet);
//		System.out.println(firstLinkedSet);
		Iterator a = thirdLinkedSet.iterator();
		while (a.hasNext()) {
			System.out.println(a.next());

		}

//		System.out.println(firstLinkedSet);
	}

}

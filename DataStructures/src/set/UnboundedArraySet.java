package set;

import java.util.Iterator;

import iterators.ArrayIterator;

/**
 * UnboundedArraySet array collection of Set (growable size)
 * 
 * UnboundedArraySet.java
 * 
 * @author Aziz Uddin
 * 
 * @author S H S Wong
 * 
 */
public class UnboundedArraySet<T> extends AbstractSetADT<T> {

	// this will be the default size of BoundedArraySet
	private static final int DEFAULT_CAPACITY = 100;
	// BoundedArraySet containing the Set
	private T[] contents;
	// BoundedArraySet max size
	private final int maxSize;
	// BoundedArraySet current size
	private int currentSize;

	// constructor with given size
	public UnboundedArraySet(int maxSize) {
		this.maxSize = maxSize;
		contents = (T[]) (new Object[maxSize]);
		currentSize = 0;

	}

	// constructor with default size
	public UnboundedArraySet() {
		this(DEFAULT_CAPACITY);
	}

	/*
	 * adds element to BoundedArraySet
	 */
	@Override
	public boolean add(T element) {
		boolean added = false;
		// checks that element is not present already on BoundedArraySet
		// as you know Set does not allow same values to be stored
		if (!contains(element)) {
			// check ArraySet size
			if (size() == contents.length)
				expandCapacity();// increase size
			// store element
			contents[currentSize] = element;
			// increase current size
			currentSize++;
			added = true;
		}
		return added;
	}

	/*
	 * removes elements to BoundedArraySet
	 */
	@Override
	public boolean remove(T element) {
		// go through all BoundedArraySet
		for (int i = 0; i < currentSize; i++) {
			// find the element
			if (element.equals(contents[i])) {
				// decrease BoundedArraySet size
				currentSize--;
				// because set is unsorted structure
				// we can swap the last element (contents[currentSize]) with
				// contents[i].
				contents[i] = contents[currentSize];
				return true;
			}
		}
		return false;
	}

	/*
	 * checks if element is on BoundedArraySet
	 */
	@Override
	public boolean contains(T element) {
		// go through each element
		for (int i = 0; i < currentSize; i++) {
			// if equals to the element return true
			if (element.equals(contents[i]))
				return true;
		}
		return false;
	}

	/*
	 * return true if BoundedArraySet current size is 0
	 */
	@Override
	public boolean isEmpty() {
		return (currentSize == 0);
	}

	/*
	 * return the current size of BoundedArraySet
	 */
	@Override
	public int size() {
		return currentSize;
	}

	/*
	 * this allows the array to iterated
	 * 
	 * @returns an Iterator which will allow user to iterate over BoundedArraySet
	 */
	@Override
	public Iterator<T> iterator() {
		// creates an ArrayIterator by taking array (contents)
		// and array current size (currentSize)
		return new ArrayIterator<T>(contents, currentSize);
	}

	/*
	 * increases ArraySet size
	 */
	public void expandCapacity() {
		// Create a new, larger array.
		T[] larger = (T[]) (new Object[contents.length * 2]);
		// Copy elements in the old array to the new array.
		for (int index = 0; index < contents.length; index++) {
			larger[index] = contents[index];
		}
		// Update the contents using the new array.
		contents = larger;
	}

	/*
	 * creates a new Set containing elements which are present in this Set and Set A
	 * 
	 */
	@Override
	public SetADT<T> intresection(SetADT<T> A) {
		// create a new empty set to hold result
		UnboundedArraySet<T> result = new UnboundedArraySet<T>(maxSize);
		for (int i = 0; i < currentSize; i++) {
			// Go through each element of this set.
			// If this element is also in A, add it to the result.
			if (A.contains(contents[i])) {
				result.contents[result.currentSize] = contents[i];
				result.currentSize++;
			}
		}
		return result;
	}

	/*
	 * Returns a new ArraySet object with exactly the same elements as this set.
	 */
	private UnboundedArraySet<T> copy() {
		// create a new empty set to hold result
		UnboundedArraySet<T> result = new UnboundedArraySet<T>(maxSize);

		// Copy the elements in this set to the result set one by one.
		for (int i = 0; i < currentSize; i++) {
			result.contents[i] = contents[i];
		}
		result.currentSize = currentSize;

		return result;
	}

	/*
	 * creates a new Set with elements of this Set and Set A
	 */
	@Override
	public SetADT<T> union(SetADT<T> A) {
		// Create a new empty set to hold result and copy the contents of this set to
		// it.
		UnboundedArraySet<T> result = copy();
		// add all the elements of set A to it
		result.addAll(A);
		return result;
	}

	/*
	 * creates a new Set which are not in common between this Set and Set A
	 */
	@Override
	public SetADT<T> difference(SetADT<T> A) {
		// create a new empty set to hold result
		UnboundedArraySet<T> result = new UnboundedArraySet<T>(maxSize);

		for (int i = 0; i < currentSize; i++) {
			/*
			 * Go through each element of this set. If this element is NOT in A, add it to
			 * the result.
			 */
			if (!A.contains(contents[i])) {
				result.contents[result.currentSize] = contents[i];
				result.currentSize++;
			}
		}
		return result;
	}

}

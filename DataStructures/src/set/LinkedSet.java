package set;

import java.util.Iterator;

//import LinkedIterators from iterators package
import iterators.LinkedIterator;
//import LinearNode from linearNode package
import linearNode.LinearNode;

/**
 * LinkedSet.java
 * 
 * @author Aziz Uddin
 * 
 * @author S H S Wong
 * 
 * @param <T>
 */
public class LinkedSet<T> extends AbstractSetADT<T> {

	// current number of elements in the set
	private int count;
	// first node in the linked set
	private LinearNode<T> contents;

	// create an empty set
	public LinkedSet() {
		count = 0;
		contents = null;
	}

	/**
	 * @return true when an element is added to LinkedSet
	 */
	@Override
	public boolean add(T element) {
		if (!(contains(element))) {
			// create a new node for the element to be added
			LinearNode<T> tmpnode = new LinearNode<T>(element);
			// make the new node reference the current node
			tmpnode.setNext(contents);
			// make the new node first node of the list
			contents = tmpnode;
			// increase counter (which is current list size)
			count++;
			return true;
		} else
			// false element not added
			return false;
	}

	/**
	 * @return true if element is removed from list
	 */
	@Override
	public boolean remove(T element) {
		// check if first node is null
		if (contents == null)
			return false;
		else {
			// check if first node has the element you want to be removed
			if (element.equals(contents.getElement())) {
				// make the second node be the first node of the list
				contents = contents.getNext();
				// decrease counter (which is list size)
				count--;
				return true;
			}
		}
		// if element is note on first node then do the followings: search
		// for the element in the list

		// track previous node (this case previous; is the first node)
		LinearNode<T> previous = contents;
		// track current node (in this case contents; is the second node)
		LinearNode<T> current = contents.getNext();

		// until current is different than null
		while (current != null) {
			if (element.equals(current.getElement())) {
				// if element found make previous node point to the node after current node
				previous.setNext(current.getNext());
				// decrease counter
				count--;
				return true;
			} else {
				// element not found move to next node
				previous = current;
				current = current.getNext();
			}
		}
		// element not found in the list
		return false;

	}

	/**
	 * @return true if the specified element is already in LinkedSet
	 */
	@Override
	public boolean contains(T element) {
		// create a temporary node
		LinearNode<T> current = contents;
		// search the list for the element
		while (current != null) {
			// if element equal to element present in contents
			if (element.equals(current.getElement()))
				return true;
			else
				// element not found move to next node
				current = current.getNext();
		}
		// if element not found in current list
		return false;
	}

	/**
	 * @return true if LinkedSet is empty
	 */
	@Override
	public boolean isEmpty() {
		return contents == null;
	}

	/**
	 * @return LinkedSet current size
	 */
	@Override
	public int size() {
		return count;
	}

	@Override
	public LinkedSet<T> intresection(SetADT<T> A) {
		// make LinkedSet to hold the intersection Set result
		LinkedSet<T> result = new LinkedSet<T>();
		// convert the given SetADT<T> A to LinkedSet, using up casting
		LinkedSet<T> convertedSetA = (LinkedSet<T>) A;
		// givenSet; holds the first node of the given Set
		LinearNode<T> givenSet = convertedSetA.contents.getNext();
		// go through each element until givenSet is null
		while (givenSet != null) {
			// check if this Set element is equal to givenSet element
			if (this.contains(givenSet.getElement())) {
				// if element found, make givenSet node follow the first node of result
				result.add(givenSet.getElement());
				// move on to next node
				givenSet = givenSet.getNext();
			} else
				// move on to next node
				givenSet = givenSet.getNext();
		}
		return result;
	}

	/**
	 * @return a new Set that is the union of this Set and Set A
	 */
	@Override
	public LinkedSet<T> union(SetADT<T> A) {
		// create a new copy of this set
		LinkedSet<T> result = new LinkedSet<T>();

		// add all the elements of A to result
		result.addAll(A);
		return result;
	}

	/**
	 * @return a new LinkedSet<T> containing elements (this-A)
	 */
	@Override
	public LinkedSet<T> difference(SetADT<T> A) {
		// copy this LinkedSet<T>
		LinkedSet<T> result = this.copy();
		// convert SetADT<T> A to LinkedSet<T>
		LinkedSet<T> convertSetA = (LinkedSet<T>) A;
		// get first node from convertSetA
		LinearNode<T> givenSet = convertSetA.contents.getNext();
		// go through each element of LinkedSet<T> convertSetA
		while (givenSet != null) {
			// check if element is on this LinkedSet<T> Set
			if (this.contains(givenSet.getElement())) {
				// if element found remove from result
				result.remove(givenSet.getElement());
				// move in to next node
				givenSet = givenSet.getNext();
			} else
				// move in to next node
				givenSet = givenSet.getNext();

		}
		return result;
	}

	/**
	 * @return Iterator<T> which allows to iterate over the list
	 */
	@Override
	public Iterator<T> iterator() {
		return new LinkedIterator<T>(contents);
	}

	/**
	 * Empties LinkedSet
	 */
	public void clear() {
		contents = null;
		count = 0;
	}

	/**
	 * @return a copy of this LinkedSet
	 */
	public LinkedSet<T> copy() {
		// create a new empty set for the result
		LinkedSet<T> result = new LinkedSet<T>();
		// track current node passing the this first node (which is contents)
		LinearNode<T> current = contents;
		// temporary new node
		LinearNode<T> tempNode;

		while (current != null) {
			// create a new node containing current element
			tempNode = new LinearNode<T>(current.getElement());
			// make this new node point to the old result
			tempNode.setNext(result.contents);
			// update result list to point to the new node
			result.contents = tempNode;
			// move on to the next node of this set
			current = current.getNext();

		}
		// define result counter size
		result.count = this.count;
		return result;
	}

}

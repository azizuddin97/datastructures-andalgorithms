package set;

/**
 * Set collection interface
 * 
 * SetADT.java
 * 
 * @author Aziz Uddin
 * 
 * @author S H S Wong
 * 
 */
public interface SetADT<T> extends Iterable<T> {

	// add new element into Set
	public boolean add(T element);

	// adds element of set a to this Set
	public boolean addAll(SetADT<T> A);

	// removes all the elements from this set except those in set A.
	public boolean removeAll(SetADT<T> A);

	// removes and returns true if element is removed from Set
	public boolean remove(T element);

	// check if element is contained in the structure
	public boolean contains(T element);

	// Returns true if this set contains no elements
	public boolean isEmpty();

	// return Set size
	public int size();

	// create a new Set containing elements which are present in this Set and Set A
	public SetADT<T> intresection(SetADT<T> A);

	// removes all the elements from this set except those in set A.
	public boolean retainAll(SetADT<T> A);

	// returns a new set equal to $(this \cup A)
	public SetADT<T> union(SetADT<T> A);

	// returns a new set equal to $(this \setminus A)$
	public SetADT<T> difference(SetADT<T> A);

	// returns true if $this \ subset eq A$
	public boolean isSubset(SetADT<T> A);

	// returns true if this set and the set A contain
	public boolean equals(SetADT<T> A);
}

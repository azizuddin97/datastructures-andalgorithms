package queue;

/**
 * UnboundedCircularArrayQueue array collection of queue (growable)
 * 
 * UnboundedCircularArrayQueue.java
 * 
 * @author Aziz Uddin
 * 
 * @author S H S Wong
 * 
 */
public class UnboundedCircularArrayQueue<T> implements QueueADT<T> {

	// default BoundedCircularArrayQueue capacity
	private static final int DEFAULT_CAPACITY = 100;
	// rear index of array
	private int rear;
	// front index of array
	private int front;
	// array
	private T[] queue;
	// counter of array current size
	private int counter;
	// maximum size of array
	private final int maxSize;

	public UnboundedCircularArrayQueue(int maxSize) {
		this.maxSize = maxSize;
		rear = 0;
		front = 0;
		counter = 0;
		queue = (T[]) (new Object[maxSize]);
	}

	public UnboundedCircularArrayQueue() {
		this(DEFAULT_CAPACITY);
	}

	/**
	 * adds new element to rear queue
	 */
	@Override
	public void enqueue(T element) {
		// check if BoundedCircularArrayQueue is full
		if (size() == queue.length)
			// expand capacity of array
			expandCapacity();
		// add element
		queue[rear] = element;
		// define new rear index position
		rear = (rear + 1) % queue.length;
		// increment counter (which is current size)
		counter++;

	}

	/**
	 * @return front element and remove from queue
	 */
	@Override
	public T dequeue() {
		// check if quque is empty
		if (isEmpty())
			throw new IllegalStateException("queue empty");
		// extract front element
		T result = queue[front];
		// make front array position null
		queue[front] = null;
		// define new front index position using the following formula
		front = (front + 1) % queue.length;
		// decrease counter (which is current size)
		counter--;
		// return element
		return result;
	}

	/**
	 * @return front queue element without removing it
	 */
	@Override
	public T first() {
		// check if empty
		if (isEmpty())
			throw new IllegalStateException("queue empty");
		// return front element
		return queue[front];
	}

	/**
	 * @return true when Circular Array is empty
	 */
	@Override
	public boolean isEmpty() {
		return (counter == 0);
	}

	/**
	 * @return current size
	 */
	@Override
	public int size() {
		return counter;
	}

	private void expandCapacity() {
		// create a new array with double size
		T[] larger = (T[]) (new Object[queue.length * 2]);
		// go through previous array
		for (int scan = 0; scan < counter; scan++) {
			// copy element from front array
			larger[scan] = queue[front];
			// define front index positio
			front = (front + 1) % queue.length;
		}
		// put front back to position 0
		front = 0;
		// rear is equal to counter (which now represent last index of array)
		rear = counter;
		// make larger array to queue array
		queue = larger;
	}
}

package queue;

// import LinearNode class from linearNode package
import linearNode.LinearNode;

/**
 * LinkedPriorityQueue.java (element are added based on their priorities) [0
 * lowest priority]
 * 
 * @author Aziz Uddin
 * 
 * @author S H S Wong
 * 
 * @param <T>
 */
public class LinkedPriorityQueue<T extends Prioritised> implements QueueADT<T> {

	// front node of the queue
	private LinearNode<T> front;

	// current size of the queue
	private int counter;

	/**
	 * initialise constructor
	 */
	public LinkedPriorityQueue() {
		front = null;
		counter = 0;
	}

	/**
	 * adds element on to queue based on their priority
	 */
	@Override
	public void enqueue(T element) {
		// create node to store element
		LinearNode<T> tmp = new LinearNode<T>(element);

		// track previous node
		LinearNode<T> previous = null;
		// track current node (which now is front node)
		LinearNode<T> current = front;

		// find where to insert the new element in the queue
		// search until current element is null
		while (current != null &&
		// and check that current element priority is higher or equal to given element
		// priority
				current.getElement().getPriority() >= element.getPriority()) {

			// make previous the current node
			previous = current;
			// move current to next node
			current = current.getNext();
		}

		// if list is empty
		if (previous == null) {
			// make tmp follow front node
			tmp.setNext(front);
			// update list making tmp; front node
			front = tmp;
		} else {
			// insert new node after previous node
			tmp.setNext(previous.getNext());
			// make previous link with tmp
			previous.setNext(tmp);
		}
		// increment list size
		counter++;

	}

	/**
	 * @return front element if list not empty
	 */
	@Override
	public T dequeue() {
		if (isEmpty())
			throw new IllegalStateException("Queue empty");
		// get element from front node
		T item = front.getElement();
		// make front node link with the front node
		front = front.getNext();
		// decrease size
		counter--;
		// return element
		return item;
	}

	/**
	 * @return front node element without removing from list
	 */
	@Override
	public T first() {
		if (isEmpty())
			throw new IllegalStateException("Queue empty");
		return front.getElement();
	}

	/**
	 * @return true if list is empty
	 */
	@Override
	public boolean isEmpty() {
		return (front == null);
	}

	/**
	 * @return list current size
	 */
	@Override
	public int size() {
		return counter;
	}

}

package queue;

/**
 * 
 * PriorityQueue.java; queue made of an array where index is the priority.
 * Moreover, each array position has a LinkedQueue list containing element of
 * same priority.
 * 
 * @author Aziz Uddin
 * 
 * @author S H S Wong
 *
 * @param <T>
 */
public class PriorityQueue<T extends Prioritised> implements QueueADT<T> {

	// declare an array of QueueADT<T>
	private QueueADT<T>[] queueList;
	// the following will represent priorities from 0 to maxPriority
	private final int maxPriority;
	// counter for current size of Array
	private int counter;

	public PriorityQueue(int maxPriority) {
		// initialise max priority
		this.maxPriority = maxPriority;
		// initialise queueList
		queueList = (QueueADT<T>[]) new Object[maxPriority + 1];

		// create a new LinkedQueue<T>() for each position of queueList array
		for (int i = 0; i < maxPriority; i++) {
			queueList[i] = new LinkedQueue<T>();
		}

		// initialise counter
		counter = 0;

	}

	/**
	 * adds element based on its priority
	 */
	@Override
	public void enqueue(T element) {
		// get element priority
		int index = element.getPriority();
		// increment counter (current size)
		counter++;
		// add element in the designated index
		queueList[index].enqueue(element);
	}

	/**
	 * @return highest priority element from queueList
	 */
	@Override
	public T dequeue() {
		// check if queueList is empty
		if (isEmpty())
			throw new IllegalStateException("Queue empty");
		// get index of highest priority
		int index = findQueue();
		// decrease counter (current size of queueList)
		counter--;
		// return element
		return queueList[index].dequeue();

	}

	/**
	 * @return highest priority element without removing from queueList
	 */
	@Override
	public T first() {
		// check if queueList empty
		if (isEmpty())
			throw new IllegalStateException("Queue empty");
		// get index of highest priority
		int index = findQueue();
		// return element
		return queueList[index].first();

	}

	/**
	 * @return true if queueList is empty
	 */
	@Override
	public boolean isEmpty() {
		return (counter == 0);
	}

	/**
	 * @return queueList current size
	 */
	@Override
	public int size() {
		return counter;
	}

	private int findQueue() {
		// go through each array position starting from highest position (maxPriority)
		for (int i = maxPriority; i >= 0; i--) {
			// check that this position is not empty
			if (!queueList[i].isEmpty())
				// return index where there is LinkedQueue element
				return i;
		}
		// queueList empty
		return -1;
	}

}

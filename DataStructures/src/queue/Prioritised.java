package queue;

/**
 * 
 * @author Aziz Uddin
 * 
 * @author S H S Wong
 * 
 *         We ensure that items can only be added to a priority queue if they
 *         have an associated priority level by demanding that the data type T
 *         must implement an interface Prioritised.
 * 
 * 
 *         To ensure that LinkedPriorityQueue is instantiated only with a type T
 *         which implements Prioritised, the type variable in the class header
 *         must be qualified as: <T extends Prioritised>
 * 
 *         This enables us to call getPriority from the body of methods of the
 *         generic class LinkedPriorityQueue.
 * 
 * 
 */
public interface Prioritised {
	// get element priority
	int getPriority();

	// set priority
	void setPriority(int level);

	// 0 is the lowest priority up to a maximum priority value M.
}

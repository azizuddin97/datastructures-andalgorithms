package queue;

/**
 * BoundedArrayQueue array collection of queue
 * 
 * BoundedArrayQueue.java
 * 
 * @author Aziz Uddin
 * 
 * @author S H S Wong
 * 
 */
public class BoundedArrayQueue<T> implements QueueADT<T> {

	private static final int DEFAULT_CAPACITY = 100;
	private int rear;
	private T[] queue;
	private final int maxSize;

	public BoundedArrayQueue(int maxSize) {
		this.maxSize = maxSize;
		rear = 0;
		queue = (T[]) (new Object[maxSize]);
	}

	public BoundedArrayQueue() {
		this(DEFAULT_CAPACITY);
	}

	/*
	 * adds element to rear of queue
	 */
	@Override
	public void enqueue(T element) {
		// check if BoundedArrayQueue is full
		if (size() == queue.length)
			throw new IllegalStateException("Queue full in enqueue");
		// add element
		queue[rear] = element;
		// increment rear count
		rear++;
	}

	/*
	 * get element from front queue
	 */
	@Override
	public T dequeue() {
		if (isEmpty())
			throw new IllegalStateException("Queue empty in dequeue");
		// get first element of ArrayQueue
		T result = queue[0];
		// decrease rear--
		rear--;

		// shift the elements as the first index ArraySet [0] is gone
		for (int scan = 0; scan < rear; scan++)
			queue[scan] = queue[scan + 1];
		// set last position to null as no longer is needed
		queue[rear] = null;
		// return result queue[0]
		return result;
	}

	/*
	 * return first element of queue without removing it from BoundedArraySet
	 */
	@Override
	public T first() {
		if (isEmpty())
			throw new IllegalStateException("Queue empty in dequeue");
		// return element
		return queue[0];
	}

	@Override
	public boolean isEmpty() {
		return (rear == 0);
	}

	@Override
	public int size() {
		return rear;
	}

	/*
	 * return maximum capacity of BoundedArraySet
	 */
	public int capacity() {
		return maxSize;
	}

}

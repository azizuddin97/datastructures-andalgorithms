package queue;

/**
 * BoundedCircularArrayQueue array collection of queue
 * 
 * BoundedCircularArrayQueue.java
 * 
 * @author Aziz Uddin
 * 
 * @author S H S Wong
 * 
 */
public class BoundedCircularArrayQueue<T> implements QueueADT<T> {

	// default BoundedCircularArrayQueue capacity
	private static final int DEFAULT_CAPACITY = 100;
	// rear index of array
	private int rear;
	// front index of array
	private int front;
	// array
	private T[] queue;
	// counter of array current size
	private int counter;
	// maximum size of array
	private final int maxSize;

	public BoundedCircularArrayQueue(int maxSize) {
		this.maxSize = maxSize;
		rear = 0;
		front = 0;
		counter = 0;
		queue = (T[]) (new Object[maxSize]);
	}

	public BoundedCircularArrayQueue() {
		this(DEFAULT_CAPACITY);
	}

	/**
	 * adds new element to rear queue
	 */
	@Override
	public void enqueue(T element) {
		// check if BoundedCircularArrayQueue is full
		if (size() == queue.length)
			throw new IllegalStateException("Queue full in enqueue");
		// add element
		queue[rear] = element;
		// define new rear index position
		rear = (rear + 1) % queue.length;
		// increment counter (which is current size)
		counter++;

	}

	/**
	 * @return front element and remove from queue
	 */
	@Override
	public T dequeue() {
		// check if quque is empty
		if (isEmpty())
			throw new IllegalStateException("queue empty");
		// extract front element
		T result = queue[front];
		// make front array position null
		queue[front] = null;
		// define new front index position using the following formula
		front = (front + 1) % queue.length;
		// decrease counter (which is current size)
		counter--;
		// return element
		return result;
	}

	/**
	 * @return front queue element without removing it
	 */
	@Override
	public T first() {
		// check if empty
		if (isEmpty())
			throw new IllegalStateException("queue empty");
		// return front element
		return queue[front];
	}

	/**
	 * @return true when Circular Array is empty
	 */
	@Override
	public boolean isEmpty() {
		return (counter == 0);
	}

	/**
	 * @return current size
	 */
	@Override
	public int size() {
		return counter;
	}

}

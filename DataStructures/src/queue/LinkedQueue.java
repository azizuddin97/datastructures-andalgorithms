package queue;

import linearNode.LinearNode;

/**
 * 
 * @author Aziz Uddin
 * @author S H S Wong
 *
 * @param <T>
 */
public class LinkedQueue<T> implements QueueADT<T> {

	// front node of the queue
	private LinearNode<T> front;
	// rear node of the queue
	private LinearNode<T> rear;
	// current size of queue
	private int count;

	public LinkedQueue() {
		// set size
		count = 0;
		// set front
		front = null;
		// set rear
		rear = null;
	}

	/**
	 * adds element to the rear of the queue
	 * 
	 * @param elemnt to be stored
	 */

	@Override
	public void enqueue(T element) {
		// create a temporary LinearNode<T> storing @param element
		LinearNode<T> temp = new LinearNode<T>(element);
		// check if empty
		if (isEmpty())
			// put temporary LinearNode<T> as front queue
			front = temp;
		else
			// add the node after the old tail node
			rear.setNext(temp);
		// update rear to point the new node
		rear = temp;
		// increase counter
		count++;

	}

	/**
	 * removes element form front
	 * 
	 * @return T which is element stored in front queue
	 */
	@Override
	public T dequeue() {
		if (isEmpty())
			throw new IllegalStateException("Queue empty in dequeue");
		// get front element
		T item = front.getElement();
		// make front to pint to new node
		front = front.getNext();
		// decrease size
		count--;

		// if front is now null
		if (front == null)
			// set rear to null as well
			rear = null;
		// return element
		return item;
	}

	/**
	 * get element from front queue without removing it
	 * 
	 * @return T which is element stored in front queue
	 */
	@Override
	public T first() {
		// check if empty
		if (isEmpty())
			throw new IllegalStateException("Queue empty in dequeue");
		// get element from front queue
		return front.getElement();
	}

	/**
	 * @return true when front is null
	 */
	@Override
	public boolean isEmpty() {
		return front == null;
	}

	/**
	 * @return current size of LinkedQueue
	 */
	@Override
	public int size() {
		return count;
	}

}

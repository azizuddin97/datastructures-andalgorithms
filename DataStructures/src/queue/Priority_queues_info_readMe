SHS Wong Data Structures and Algorithms
---------------------------------------------------------------------			
				Priority Queues

• 	Most queues are first-in, first-out (FIFO) data structures, but
	sometimes it is useful for high priority data to ‘jump’ the queue and
	join a queue ahead of lower priority data.

• 	Data items of the same priority are still processed in a FIFO manner.

• 	T	he priority of data items is represented by a range of non-negative
	integer values.
 		0 is the lowest priority up to a maximum priority value M.

• 	A priority queue can be implemented using a linked list or an array.
		Elements are ordered by priority level of the data items involved.

• 	To enqueue an item means to insert it into a position in the list/array
	ahead of all lower priority items, but behind data items of higher or
	equal priority.		
	
---------------------------------------------------------------------	
				Priority Queue Implementation
	
	
• 	We ensure that items can only be added to a priority queue if they
	have an associated priority level by demanding that the data type T
	must implement an interface Prioritised, e.g.:

	public interface Prioritised {
		int getPriority();
		void setPriority(int level);
	}
	
• 	To ensure that LinkedPriorityQueue is instantiated only with a
	type T which implements Prioritised, the type variable in the
	class header must be qualified as:
	
		<T extends Prioritised>
		
	This enables us to call getPriority from the body of methods of
	the generic class LinkedPriorityQueue.

	
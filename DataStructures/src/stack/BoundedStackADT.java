package stack;

/**
 * BoundedStackADT abstract class for bounded stack
 * 
 * BoundedStackADT.java
 * 
 * @author Aziz Uddin
 */
public interface BoundedStackADT<T> extends StackADT<T> {

	// Returns true if Stack is full
	public boolean isFull();

	// Return the maximum capacity of Stack
	public int capacity();
}

package stack;

import java.util.EmptyStackException;

/**
 * UnboundedArrayStack array collection of Stack (growable size)
 * 
 * UnboundedArrayStack.java
 * 
 * @author Aziz Uddin
 * 
 * @author S H S Wong
 * 
 */
public class UnboundedArrayStack<T> implements StackADT<T> {

	private static int defaultSize = 100;
	private T[] stack;
	private int top;
	private int maximumSize;

	// Construct an ArrayStack of given size
	public UnboundedArrayStack() {
		this.top = 0;
		this.maximumSize = defaultSize;
		// array needs to be converted in Object as generic class do not support Arrays
		this.stack = (T[]) (new Object[this.maximumSize]);
	}

	@Override
	public boolean push(T element) {
		if (stack.length - 1 == top) {
			// if top has reached the stack size, the follwing will increase ArrayStack size
			this.increseStackSize();
		}
		stack[top] = element;
		top++;
		return true;
	}

	@Override
	public T pop() {
		// Check if ArrayStuck is empty
		if (isEmpty()) {
			throw new EmptyStackException();
		} else {
			// get value from top and store in a temporary variable
			T temp = stack[top];
			// make the value on top null;
			stack[top] = null;
			// decrease top
			top--;
			return temp;
		}
	}

	@Override
	public T peek() {
		// Check if ArrayStuck is empty
		if (isEmpty())
			throw new EmptyStackException();
		else
			return stack[top];
	}

	/*
	 * return the current size of ArrayStack
	 */
	@Override
	public int size() {
		return top;
	}

	@Override
	public boolean isEmpty() {
		return (top == 0);
	}

	private boolean increseStackSize() {
		// Doubles the Maximum size
		this.maximumSize = this.maximumSize * 2;
		// Create a temporary ArrayStack
		T[] tempStack = (T[]) (new Object[this.maximumSize]);
		// Copys all the elements from stack and stores in temporary ArrayStack
		// (tempStack)
		for (int c = 0; c < stack.length - 1; c++) {
			tempStack[c] = stack[c];
		}
		// tempStack becomes the main Stack
		stack = tempStack;
		return true;
	}

	@Override
	public void clear() {
		top = 0;

	}

}

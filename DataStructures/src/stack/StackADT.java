package stack;

/**
 * Stack collection interface
 * 
 * StackADT.java
 * 
 * @author Aziz Uddin
 * 
 * @author S H S Wong
 * 
 */
public interface StackADT<T> {

	// Adds element to the top of Stack and returns true if element is added
	public boolean push(T element);

	// Returns and removes element from the top of the Stack
	public T pop();

	// Returns the top element from Stack without removing it
	public T peek();

	// Clear Stack
	public void clear();

	// Returns the number element in the stack;
	public int size();

	// Returns true if Stack is empty (no elements)
	public boolean isEmpty();

}

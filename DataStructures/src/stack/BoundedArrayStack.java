package stack;

import java.util.EmptyStackException;

/*
 * BoundedArrayStack array collection of Stack
 * 
 * BoundedArrayStack.java
 * 
 * @author Aziz Uddin
 * 
 * @author S H S Wong
 * 
 */
public class BoundedArrayStack<T> implements BoundedStackADT<T> {

	private static int defaultSize = 100;
	private T[] stack;
	private int top;
	private int maximumSize;

	// Construct an ArrayStack of given size
	public BoundedArrayStack(int maximumSize) {
		this.top = 0;
		this.maximumSize = maximumSize;
		// array needs to be converted in Object as generic class do not support Arrays
		this.stack = (T[]) (new Object[this.maximumSize]);
	}

	// Construct an ArrayStack of default size (100)
	public BoundedArrayStack() {
		this(defaultSize);
	}

	@Override
	public boolean push(T element) {
		// Check if ArrayStuck is full
		if (!isFull()) {
			stack[top] = element;
			top++;
			return true;
		} else
			return false;

	}

	@Override
	public T pop() {
		// Check if ArrayStuck is empty
		if (isEmpty()) {
			throw new EmptyStackException();
		} else {
			// get value from top and store in a temporary variable
			T temp = stack[top];
			// make the value on top null;
			stack[top] = null;
			// decrease top
			top--;
			return temp;
		}
	}

	@Override
	public T peek() {
		// Check if ArrayStuck is empty
		if (isEmpty())
			throw new EmptyStackException();
		else
			return stack[top];
	}

	@Override
	public int size() {
		return top;
	}

	@Override
	public boolean isEmpty() {
		return (top == 0);
	}

	@Override
	public boolean isFull() {
		return (stack.length - 1 == top);
	}

	@Override
	public int capacity() {
		return maximumSize;
	}

	@Override
	public void clear() {
		top = 0;
	}

}

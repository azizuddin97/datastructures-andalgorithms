package stack;

import linearNode.LinearNode;

/**
 * 
 * @author aziz uddin
 * @author S H S Wong
 * @param <T>
 */
public class LinkedStack<T> implements StackADT<T> {

	// node representing stack top
	private LinearNode<T> top;
	// stack counter
	private int count;

	public LinkedStack() {
		// define top stack to null
		top = null;
		// LinkedStack counter
		count = 0;
	}

	/**
	 * adds element top of LinkedStack
	 */
	@Override
	public boolean push(T element) {
		// create a temporary LinearNode by passing @param element
		LinearNode<T> temp = new LinearNode<T>(element);
		// makes temp follower of tLinearNode
		temp.setNext(top);
		// make temp the top LinearNode
		top = temp;
		// increment counter
		count++;
		// return true when added new element
		return true;
	}

	/*
	 * removes element from top LinearNode
	 */
	@Override
	public T pop() {
		// check if empty
		if (isEmpty())
			throw new IllegalStateException("Stack empty in pop");
		// get top element from stack
		T result = top.getElement();
		// links the top to next LinearNode
		top = top.getNext();
		// decrease counter
		count--;
		// return result
		return result;
	}

	/*
	 * get top LinearNode element
	 */
	@Override
	public T peek() {
		// check if empty
		if (isEmpty())
			throw new IllegalStateException("Stack empty in pop");
		// return element from top
		return top.getElement();
	}

	/*
	 * clear LinearNode
	 */
	@Override
	public void clear() {
		top = null;

	}

	/*
	 * return LinkedStack size
	 */
	@Override
	public int size() {
		return count;
	}

	/*
	 * return true if LinkedStack is empty
	 */
	@Override
	public boolean isEmpty() {
		return top == null;
	}

}
